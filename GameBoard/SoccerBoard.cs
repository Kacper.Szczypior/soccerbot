﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace GameBoard
{
    public class SoccerBoard
    {
        public int length0;
        public int length1;
        private readonly Node[,] nodes;

        public SoccerBoard(int x, int y)
        {
            nodes = new Node[x + 1, y + 3];
            for (int i = 0; i < x + 1; i++)
            {
                for (int j = 0; j < y + 3; j++)
                {
                    nodes[i, j] = new Node
                    {
                        X = i,
                        Y = j,
                    };
                }
            }

            length0 = nodes.GetLength(0);
            length1 = nodes.GetLength(1);

            CreateEdges();
            DrawUnnecessary();
        }

        public Node[,] GetNodes
        {
            get
            {
                return nodes;
            }
        }

        public Node GetNode(Point point)
        {
            return GetNode(point.X, point.Y);
        }

        public Node GetNode(int x, int y)
        {
            return nodes[x, y];
        }

        public Node GetNodeSafe(int x, int y)
        {
            if (x < 0 || y < 0 || x >= nodes.GetLength(0) || y >= nodes.GetLength(1))
                return null;
            return nodes[x, y];
        }

        public void RemoveEdge(int x, int y, int addX, int addY)
        {
            if (GetNodeSafe(x, y) != null && GetNodeSafe(x + addX, y + addY) != null)
            {
                var edge = GetNodeSafe(x, y).Nodes.FirstOrDefault(u => u.Node == GetNodeSafe(x + addX, y + addY));
                if (edge != null)
                    GetNodeSafe(x, y).Nodes.Remove(edge);
                //edge.drawn = true;
                edge = GetNodeSafe(x + addX, y + addY).Nodes.FirstOrDefault(u => u.Node == GetNodeSafe(x, y));
                if (edge != null)
                    GetNodeSafe(x + addX, y + addY).Nodes.Remove(edge);
                //edge.drawn = true;
            }
        }

        private void CreateEdges()
        {
            for (int i = 0; i < nodes.GetLength(0); i++)
            {
                for (int j = 0; j < nodes.GetLength(1); j++)
                {
                    CreateEdgesForNode(i, j);
                }
            }
        }

        private void CreateEdgesForNode(int x, int y)
        {
            foreach (Node node in GetNodesAround(x, y))
            {
                GetNodeSafe(x, y).Nodes.Add(new Edge(node));
            }
        }

        private void DrawUnnecessary()
        {
            for (int i = 0; i < nodes.GetLength(0); i++)
            {
                if (i == nodes.GetLength(0) / 2)
                    continue;
                MarkAllEdgesFromNode(i, 0);
                MarkAllEdgesFromNode(i, nodes.GetLength(1) - 1);
            }
            for (int i = 0; i < nodes.GetLength(0); i++)
            {
                if (i == nodes.GetLength(0) / 2 || i == nodes.GetLength(0) / 2 - 1)
                    continue;
                RemoveEdge(i, 1, 1, 0);
                GetNodeSafe(i, 1).Set = true;
                RemoveEdge(i, nodes.GetLength(1) - 2, 1, 0);
                GetNodeSafe(i, nodes.GetLength(1) - 2).Set = true;
            }
            GetNodeSafe(nodes.GetLength(0) / 2 - 1, 1).Set = true;
            GetNodeSafe(nodes.GetLength(0) / 2 - 1, nodes.GetLength(1) - 2).Set = true;

            for (int j = 0; j < nodes.GetLength(1); j++)
            {
                RemoveEdge(0, j, 0, 1);
                GetNodeSafe(0, j).Set = true;
                RemoveEdge(nodes.GetLength(0) - 1, j, 0, 1);
                GetNodeSafe(nodes.GetLength(0) - 1, j).Set = true;
            }
        }

        #region init

        private IEnumerable<Node> GetNodesAround(int x, int y)
        {
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (i == 0 && j == 0)
                        continue;
                    var node = GetNodeSafe(x + i, y + j);
                    if (node != null)
                        yield return node;
                }
            }
        }

        private void MarkAllEdgesFromNode(int x, int y)
        {
            GetNode(x, y).Nodes = new List<Edge>();//this removes ;-)
            foreach (Node node in GetNodesAround(x, y))
            {
                var secondEdge = node.Nodes.FirstOrDefault(u => u.Node == GetNode(x, y));
                if (secondEdge != null)
                    //secondEdge.drawn = true;
                    node.Nodes.Remove(secondEdge);
            }
        }

        #endregion init
    }
}
﻿namespace GameBoard
{
    public class Edge
    {
        public Edge(Node node)
        {
            Node = node;
        }

        public bool Drawn { get; set; }
        public Node Node { get; set; }
    }
}

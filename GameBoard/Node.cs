﻿using System.Collections.Generic;

namespace GameBoard
{
    public class Node
    {
        //TODO Add reference to node here to make setting faster??
        public bool NodeAdded { get; set; }
        public List<Edge> Nodes { get; set; } = new List<Edge>();
        public bool Searched { get; set; }
        public bool Set { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public override string ToString()
        {
            return Set.ToString();
        }
    }
}
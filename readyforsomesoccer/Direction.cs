﻿namespace readyforsomesoccer
{
    public enum Direction
    {
        up = 8,
        upleft = 7,
        left = 4,
        downleft = 1,
        down = 2,
        downright = 3,
        right = 6,
        upright = 9
    }
}

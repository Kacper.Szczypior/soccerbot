﻿using System.Collections.Generic;

namespace readyforsomesoccer
{
    public class MoveEvaluation
    {
        public short botDistance;
        public GameResult result = GameResult.noOneWins;
        public short topDistance;

        public MoveEvaluation(IEnumerable<MoveResult> moves)
        {
            foreach (var move in moves)
            {
                Moves.Add(move);
            }
        }
        public List<MoveResult> Moves { get; set; } = new List<MoveResult>();

        public static bool operator <(MoveEvaluation x, MoveEvaluation y)
        {
            if (x.result == GameResult.botPlayerWin)
            {
                if (y.result == GameResult.botPlayerWin)
                    return false;
                return true;
            }

            if (y.result == GameResult.botPlayerWin)
            {
                return false;
            }

            if (y.result == GameResult.topPlayerWin)
            {
                return true;
            }

            if (x.result == GameResult.topPlayerWin)
            {
                return false;
            }

            return x.botDistance - x.topDistance < y.botDistance - y.topDistance;
        }

        public static bool operator >(MoveEvaluation x, MoveEvaluation y)
        {
            return y < x;
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace readyforsomesoccer
{
    public partial class MainWindow : Window
    {
        private enum PlayMode
        {
            playerVSComputer,
            twoBots
        }

        private readonly PlayMode _playMode = PlayMode.playerVSComputer;

        private const int squareLength = 50;
        private const int BoardHeight = 10;
        private const int BoardWidth = 8;
        private const int margin = squareLength;
        private Point currentPoint = new Point(BoardWidth / 2, BoardHeight / 2);
        public SoccerGameWithAI soccerGame;
        public bool keyboardlock;

        private static Line GetWhiteLine(double X1, double X2, double Y1, double Y2)
        {
            return new Line
            {
                X1 = X1,
                X2 = X2,
                Y1 = Y1,
                Y2 = Y2,
                Stroke = Brushes.White,
                Visibility = Visibility.Visible
            };
        }

        private void DrawPitch()
        {
            Board.Children.Add(GetWhiteLine(margin, margin, margin, margin + BoardHeight * squareLength));
            Board.Children.Add(GetWhiteLine(margin, margin + (BoardWidth / 2 - 1) * squareLength, margin, margin));
            Board.Children.Add(GetWhiteLine(margin + (BoardWidth / 2 + 1) * (squareLength), margin + BoardWidth * squareLength, margin, margin));
            Board.Children.Add(GetWhiteLine(margin + BoardWidth * squareLength, margin + BoardWidth * squareLength, margin, margin + BoardHeight * squareLength));
            Board.Children.Add(GetWhiteLine(margin, margin + (BoardWidth / 2 - 1) * squareLength, margin + BoardHeight * squareLength, margin + BoardHeight * squareLength));
            Board.Children.Add(GetWhiteLine(margin + (BoardWidth / 2 + 1) * (squareLength), margin + BoardWidth * squareLength, margin + BoardHeight * squareLength, margin + BoardHeight * squareLength));
        }

        private void DrawPoints()
        {
            for (int i = 1; i < BoardHeight; i++)
            {
                for (int j = 1; j < BoardWidth; j++)
                {
                    var rect = new Rectangle
                    {
                        Stroke = Brushes.White,
                        Visibility = Visibility.Visible,
                        Width = 3,
                        Height = 3,
                        Fill = Brushes.White
                    };

                    Board.Children.Add(rect);
                    Canvas.SetLeft(rect, margin + squareLength * j - 1);
                    Canvas.SetTop(rect, margin + squareLength * i - 1);
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            soccerGame = new SoccerGameWithAI(BoardWidth, BoardHeight);
            Board.Background = Brushes.Green;
            DrawPitch();
            DrawPoints();
            log.Text = "here will be my text";
        }

        public void SyncWithHistory(List<Direction> dir)
        {
            foreach (Direction direction in dir)
            {
                GetIntsFromDirection(direction, out int addX, out int addY);
                var currentX = margin + currentPoint.X * squareLength;
                var finalX = currentX + addX * squareLength;
                var currentY = margin + currentPoint.Y * squareLength;
                var finalY = currentY + addY * squareLength;
                Board.Children.Add(GetWhiteLine(currentX + 1, finalX + 1, currentY + 1, finalY + 1));
                currentPoint = new Point(currentPoint.X + addX, currentPoint.Y + addY);
            }
        }

        protected static void GetIntsFromDirection(Direction dir, out int addX, out int addY)
        {
            addX = 0;
            addY = 0;
            switch (dir)
            {
                case Direction.down:
                    addY = 1;
                    break;

                case Direction.downleft:
                    addY = 1;
                    addX = -1;
                    break;

                case Direction.left:
                    addX = -1;
                    break;

                case Direction.upleft:
                    addX = -1;
                    addY = -1;
                    break;

                case Direction.up:
                    addY = -1;
                    break;

                case Direction.upright:
                    addY = -1;
                    addX = 1;
                    break;

                case Direction.right:
                    addX = 1;
                    break;

                case Direction.downright:
                    addX = 1;
                    addY = 1;
                    break;
            }
        }

        public void Move(Direction dir)
        {
            keyboardlock = true;
            if (_playMode == PlayMode.twoBots)
            {
                LetAIPlayAsync();
                return;
            }

            var result = soccerGame.Move(dir);
            if (!result.worked)
            {
                keyboardlock = false;
                return;
            }
            var currentX = margin + currentPoint.X * squareLength;
            var finalX = currentX + result.addX * squareLength;
            var currentY = margin + currentPoint.Y * squareLength;
            var finalY = currentY + result.addY * squareLength;
            Board.Children.Add(GetWhiteLine(currentX + 1, finalX + 1, currentY + 1, finalY + 1));
            currentPoint = new Point(currentPoint.X + result.addX, currentPoint.Y + result.addY);
            EvaluateGameState();
            log.Text = soccerGame.GetLog();
            if (!soccerGame.botplayersTurn)
                LetAIPlayAsync();
            else keyboardlock = false;
        }

        public void EvaluateGameState()
        {
            if (soccerGame.GetGameState() != GameResult.noOneWins)
            {
                log.Text += soccerGame.GetGameState().ToString("G");
            }
        }

        private async Task LetAIPlayAsync()
        {
            var moves = soccerGame.MakeIntelligentMove();
            foreach (MoveResult result in moves.Moves)
            {
                await Task.Delay(100);
                var currentX = margin + currentPoint.X * squareLength;
                var finalX = currentX + result.addX * squareLength;
                var currentY = margin + currentPoint.Y * squareLength;
                var finalY = currentY + result.addY * squareLength;
                Board.Children.Add(GetWhiteLine(currentX + 1, finalX + 1, currentY + 1, finalY + 1));
                currentPoint = new Point(currentPoint.X + result.addX, currentPoint.Y + result.addY);
            }
            log.Text = soccerGame.GetLog();
            keyboardlock = false;
        }

        private void ResetGame()
        {
            Board.Children.Clear();
            var nives = new List<Direction> { /* Direction.up, Direction.upleft, Direction.upleft, Direction.upleft */};

            /*nives = new JavaScriptSerializer().Deserialize<List<Direction>>(
                "[8,1,7,1,7,9,3,2,7,7,6,2,1,6,1,3,9,7,6,2,3,8,7,6,1,6,8,6,1,3,9,2,9,4,7,2,7,8,4,4,2,2,1,6,8,9,9,7,2,9,2,6,7,4,7,4,3,8,6,1,6,8,8,7,1,3,9,9,2,7,2,6,9]"
                );*/

            soccerGame = new SoccerGameWithAI(BoardWidth, BoardHeight, nives);
            currentPoint = new Point(BoardWidth / 2, BoardHeight / 2);
            SyncWithHistory(nives);
            Board.Background = Brushes.Green;
            DrawPitch();
            DrawPoints();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (keyboardlock)
                return;
            log.Text = e.Key.ToString();
            switch (e.Key)
            {
                case Key.Up:
                case Key.NumPad8:
                    Move(Direction.up);
                    break;

                case Key.Right:
                case Key.NumPad6:
                    Move(Direction.right);
                    break;

                case Key.Left:
                case Key.NumPad4:
                    Move(Direction.left);
                    break;

                case Key.Down:
                case Key.NumPad2:
                    Move(Direction.down);
                    break;

                case Key.NumPad1:
                case Key.End:
                    Move(Direction.downleft);
                    break;

                case Key.NumPad3:
                case Key.PageDown:
                    Move(Direction.downright);
                    break;

                case Key.NumPad7:
                case Key.Home:
                    Move(Direction.upleft);
                    break;

                case Key.NumPad9:
                case Key.PageUp:
                    Move(Direction.upright);
                    break;

                case Key.R:
                    ResetGame();
                    break;
            }
        }
    }
}
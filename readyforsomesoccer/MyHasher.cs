﻿using System.Collections.Generic;

namespace readyforsomesoccer
{
    public class MyHasher
    {
        private readonly Dictionary<int, Dictionary<int, BoardState>> states = new Dictionary<int, Dictionary<int, BoardState>>();

        public void Add(BoardState newBoard)
        {
            if (!states.ContainsKey(newBoard.secondHashCode))
                states.Add(newBoard.secondHashCode, new Dictionary<int, BoardState>());
            states[newBoard.secondHashCode].Add(newBoard.hashCode, newBoard);
        }
        public bool Contains(BoardState second, out MoveEvaluation eval)
        {
            if (!states.ContainsKey(second.secondHashCode))
            {
                eval = null;
                return false;
            }
            if(states[second.secondHashCode].TryGetValue(second.hashCode, out BoardState value)){
                eval = value.eval;
                return true;
            }
            eval = null;
            return false;
        }
    }
}

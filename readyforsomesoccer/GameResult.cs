﻿namespace readyforsomesoccer
{
    public enum GameResult
    {
        botPlayerWin,
        topPlayerWin,
        noOneWins,
    }
}
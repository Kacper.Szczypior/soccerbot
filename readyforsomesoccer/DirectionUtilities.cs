﻿using System;

namespace readyforsomesoccer
{
    public static class DirectionUtilities
    {
        public static Direction ConvertToDirection(int addX, int addY)
        {
            if (addY == 1 && addX == 0)
                return Direction.down;
            if (addY == 1 && addX == -1)
                return Direction.downleft;
            if (addY == 0 && addX == -1)
                return Direction.left;
            if (addY == -1 && addX == -1)
                return Direction.upleft;
            if (addY == -1 && addX == 0)
                return Direction.up;
            if (addY == -1 && addX == 1)
                return Direction.upright;
            if (addY == 0 && addX == 1)
                return Direction.right;
            if (addY == 1 && addX == 1)
                return Direction.downright;
            throw new Exception("unexpected direction");
        }
    }
}

﻿using System.Collections.Generic;

namespace readyforsomesoccer
{
    public class AIMove
    {
        public List<MoveResult> Moves { get; set; } = new List<MoveResult>();
    }
}

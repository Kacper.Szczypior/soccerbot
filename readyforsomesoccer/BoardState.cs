﻿using System;
using System.Drawing;

namespace readyforsomesoccer
{
    public class BoardState
    {
        public int secondHashCode;
        private readonly bool[,,] board;
        public int hashCode;
        private readonly int seed = 123;
        public MoveEvaluation eval;

        public BoardState(int height, int width)
        {
            board = new bool[width, height, 4];
            if (HashesBoardState.Numbers == null)
            {
                var rand = new Random(seed);
                HashesBoardState.Numbers = new int[board.GetLength(0), board.GetLength(1), board.GetLength(2), 2];
                for (int x = 0; x < board.GetLength(0); x++)
                {
                    for (int y = 0; y < board.GetLength(1); y++)
                    {
                        for (int z = 0; z < board.GetLength(2); z++)
                        {
                            for (int v = 0; v < 2; v++)
                            {
                                HashesBoardState.Numbers[x, y, z, v] = rand.Next();
                            }
                        }
                    }
                }
            }
        }

        private enum BoardStateInternalDirection
        {
            right = 0,
            downright = 1,
            down = 2,
            downleft = 3
        }

        public BoardState Copy()
        {
            var toReturn = new BoardState(board.GetLength(1), board.GetLength(0))
            {
                hashCode = hashCode,
                secondHashCode = secondHashCode,
                eval = eval
            };
            Array.Copy(board, 0, toReturn.board, 0, board.Length);
            return toReturn;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var second = obj as BoardState;
            if (second == null)
                return false;
            return this.Equals(second);
        }

        public bool Equals(BoardState second)
        {
            if (second == null)
                return false;
            if (board.GetLength(0) != second.board.GetLength(0) || board.GetLength(1) != second.board.GetLength(1))
                return false;
            if (hashCode != second.hashCode)
                return false;

            for (int x = 0; x < board.GetLength(0); x++)
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    for (int z = 0; z < board.GetLength(2); z++)
                    {
                        if (second.board[x, y, z] != board[x, y, z])
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public override int GetHashCode()
        {
            return hashCode;
        }

        public void MarkOnBoardState(Point startingPoint, Direction dir, bool reset = false)
        {
            switch (dir)
            {
                case Direction.down:
                    Mark(startingPoint.X, startingPoint.Y, (int)BoardStateInternalDirection.down, reset);
                    break;

                case Direction.downleft:
                    Mark(startingPoint.X - 1, startingPoint.Y, (int)BoardStateInternalDirection.downleft, reset);
                    break;

                case Direction.left:
                    Mark(startingPoint.X - 1, startingPoint.Y, (int)BoardStateInternalDirection.right, reset);
                    break;

                case Direction.upleft:
                    Mark(startingPoint.X - 1, startingPoint.Y - 1, (int)BoardStateInternalDirection.downright, reset);
                    break;

                case Direction.up:
                    Mark(startingPoint.X, startingPoint.Y - 1, (int)BoardStateInternalDirection.down, reset);
                    break;

                case Direction.upright:
                    Mark(startingPoint.X, startingPoint.Y - 1, (int)BoardStateInternalDirection.downleft, reset);
                    break;

                case Direction.right:
                    Mark(startingPoint.X, startingPoint.Y, (int)BoardStateInternalDirection.right, reset);
                    break;

                case Direction.downright:
                    Mark(startingPoint.X, startingPoint.Y, (int)BoardStateInternalDirection.downright, reset);
                    break;
            }
        }

        private void Mark(int x, int y, int z, bool reset)
        {
            board[x, y, z] = !reset;
            hashCode = hashCode ^ HashesBoardState.Numbers[x, y, z, 0];
            secondHashCode = secondHashCode ^ HashesBoardState.Numbers[x, y, z, 1];
        }
    }
}
﻿using GameBoard;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.Script.Serialization;

namespace readyforsomesoccer
{
    public class SoccerGame : SoccerBoard
    {
        public bool botplayersTurn = true;
        public BoardState currentState;//todo private
        public readonly Point topPlayerBase;
        public readonly Point botPlayerBase;
        protected Point currentPoint;

        private readonly List<MoveResult> history = new List<MoveResult>();

        public SoccerGame(int x, int y) : base(x, y)
        {
            currentPoint = new Point(x / 2, y / 2 + 1);
            GetNode(x / 2, y / 2 + 1).Set = true;
            topPlayerBase = new Point(x / 2, length1 - 1);
            botPlayerBase = new Point(x / 2, 0);
            currentState = new BoardState(length1 + 1, length0 + 1);
            history = new List<MoveResult>();
        }

        public IEnumerable<MoveResult> MoveHistory
        {
            get
            {
                return history;
            }
        }

        public GameResult GetGameState()
        {
            return GetGameState(currentPoint);
        }

        public GameResult GetGameState(Point somePoint)
        {
            if (somePoint.X == topPlayerBase.X && somePoint.Y == topPlayerBase.Y)
                return GameResult.topPlayerWin;
            if (somePoint.X == botPlayerBase.X && somePoint.Y == botPlayerBase.Y)
                return GameResult.botPlayerWin;
            return GameResult.noOneWins;
        }

        public string GetLog()
        {
            return (botplayersTurn ? "Gracz1 : " : "Gracz2 : ") +
                new JavaScriptSerializer().Serialize(MoveHistory.Select(u => u.ConvertToDirection()));
        }

        public static IEnumerable<Edge> GetMovableEdges(Node node)
        {
            foreach (Edge someEdge in node.Nodes)
            {
                if (someEdge.Drawn)
                    continue;
                yield return someEdge;
            }
        }

        public IEnumerable<Node> GetMovableNodes(Point point)
        {
            var node = GetNode(point);
            foreach (Edge edge in GetMovableEdges(node))
            {
                yield return edge.Node;
            }
        }

        public void MarkEdge(int x, int y, int addX, int addY, bool reset = false)
        {
            foreach (Edge edge in GetNode(x, y).Nodes)
            {
                if (edge.Node != GetNode(x + addX, y + addY))
                    continue;
                else
                {
                    edge.Drawn = !reset;
                    break;
                }
            }
            foreach (Edge edge in GetNode(x + addX, y + addY).Nodes)
            {
                if (edge.Node != GetNode(x, y))
                    continue;
                else
                {
                    edge.Drawn = !reset;
                    break;
                }
            }
        }

        public void MarkMove(MoveResult move)
        {
            MarkMove(new MoveResult[] { move });
        }

        public List<MoveResult> MoveOnePlayer(IEnumerable<MoveResult> moves)
        {
            var result = new List<MoveResult>();
            var finish = false;
            foreach (MoveResult move in moves)
            {
                currentState.MarkOnBoardState(currentPoint, move.ConvertToDirection());
                MarkEdge(currentPoint.X, currentPoint.Y, move.addX, move.addY);
                currentPoint = MovePoint(currentPoint, move);
                if (!GetNode(currentPoint).Set)
                {
                    botplayersTurn = !botplayersTurn;
                    finish = true;
                }
                GetNode(currentPoint).Set = true;
                history.Add(move);
                result.Add(move);
                if (finish)
                    return result;
            }
            return result;
        }

        public void MarkMove(IEnumerable<MoveResult> moves)
        {
            foreach (MoveResult move in moves)
            {
                currentState.MarkOnBoardState(currentPoint, move.ConvertToDirection());
                MarkEdge(currentPoint.X, currentPoint.Y, move.addX, move.addY);
                currentPoint = MovePoint(currentPoint, move);
                if (!GetNode(currentPoint).Set)
                    botplayersTurn = !botplayersTurn;
                GetNode(currentPoint).Set = true;
                history.Add(move);
            }
        }

        public MoveResult Move(Direction dir)
        {
            var fail = new MoveResult
            {
                addX = 0,
                addY = 0,
                worked = false
            };

            if (GetGameState() != GameResult.noOneWins)
                return fail;

            GetIntsFromDirection(dir, out int addX, out int addY);

            var movedTo = GetNodeSafe(currentPoint.X + addX, currentPoint.Y + addY);
            if (movedTo == null)
                return fail;
            var edge = GetNode(currentPoint).Nodes.FirstOrDefault(u => u.Node == movedTo);
            if (edge == null || edge.Drawn)
            {
                return fail;
            }
            else
            {
                var result = new MoveResult
                {
                    addX = addX,
                    addY = addY,
                    worked = true
                };
                MarkMove(result);
                return result;
            }
        }

        protected static Point MovePoint(Point toMove, MoveResult moveToMake, bool revert = false)
        {
            if (revert)
                return new Point(toMove.X - moveToMake.addX, toMove.Y - moveToMake.addY);
            return new Point(toMove.X + moveToMake.addX, toMove.Y + moveToMake.addY);
        }

        private static void GetIntsFromDirection(Direction dir, out int addX, out int addY)
        {
            addX = 0;
            addY = 0;
            switch (dir)
            {
                case Direction.down:
                    addY = 1;
                    break;

                case Direction.downleft:
                    addY = 1;
                    addX = -1;
                    break;

                case Direction.left:
                    addX = -1;
                    break;

                case Direction.upleft:
                    addX = -1;
                    addY = -1;
                    break;

                case Direction.up:
                    addY = -1;
                    break;

                case Direction.upright:
                    addY = -1;
                    addX = 1;
                    break;

                case Direction.right:
                    addX = 1;
                    break;

                case Direction.downright:
                    addX = 1;
                    addY = 1;
                    break;
            }
        }
    }
}
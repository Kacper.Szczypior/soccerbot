﻿using GameBoard;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace readyforsomesoccer
{
    public class SoccerGameWithAI : SoccerGame
    {
        protected Point crawlPoint;
        protected MyHasher myHasher;

        private const int alreadyThere = 0;
        private const int notfound = 100;
        private readonly int future = 3;//must fix

        public SoccerGameWithAI(int x, int y)
            : base(x, y)
        {
        }

        public SoccerGameWithAI(int x, int y, List<Direction> moves)
            : this(x, y)
        {
            foreach (var u in moves)
            {
                Move(u);
            }
            botplayersTurn = true;
        }

        public static MoveResult MakeMoveResult(Node from, Node to)
        {
            return new MoveResult
            {
                addX = to.X - from.X,
                addY = to.Y - from.Y,
                worked = true
            };
        }

        public MoveEvaluation EvaluateBoard(List<MoveResult> moves)//higher means advantage for going top //needs to be tested
        {
            var fast = EvaluateFast(moves);
            if (fast != null)
                return fast;
            return new MoveEvaluation(moves)
            {
                topDistance = HowFarIsIt(topPlayerBase),
                botDistance = HowFarIsIt(botPlayerBase)
            };
            //comparelogic
            //if (distanceTargetMark == 0 && distanceHome == 100)
            //{
            //    return new MoveEvaluation
            //    {
            //        topDistance = CountEmptyPoint() % 2 == 0 ? -150 : 150
            //    };
            //}
            //if (distanceHome == 100)
            //{
            //    distanceTargetMark += 50;
            //}
        }

        public MoveEvaluation EvaluateFast(List<MoveResult> moves)//todo needs to check for no more moves
        {
            switch (GetGameState(crawlPoint))
            {
                case GameResult.topPlayerWin:
                    return new MoveEvaluation(moves)
                    {
                        result = GameResult.topPlayerWin
                    };

                case GameResult.botPlayerWin:
                    return new MoveEvaluation(moves)
                    {
                        result = GameResult.botPlayerWin
                    };
            }
            return null;
        }

        public AIMove MakeIntelligentMove()
        {
            if (GetGameState() != GameResult.noOneWins)
            {
                return new AIMove
                {
                    Moves = new List<MoveResult>()
                };
            }
            crawlPoint = new Point(currentPoint.X, currentPoint.Y);
            var results = new List<MoveResult>();
            myHasher = new MyHasher();
            var bestPosition = Crawlv3(results, !botplayersTurn, future);
            var toReturn = new AIMove
            {
                Moves = bestPosition.Moves
            };
            toReturn.Moves = MoveOnePlayer(toReturn.Moves);
            return toReturn;
        }

        private static MoveEvaluation ChooseBetter(bool goingBot, MoveEvaluation currentBest, MoveEvaluation candidate)
        {
            if (currentBest == null)
            {
                currentBest = candidate;
            }
            else
            {
                if (goingBot)
                {
                    if (candidate > currentBest)
                    {
                        currentBest = candidate;
                    }
                }
                else
                {
                    if (candidate < currentBest)
                    {
                        currentBest = candidate;
                    }
                }
            }

            return currentBest;
        }

        private int CountEmptyPoint()
        {
            var foundPoints = new bool[length0, length1];
            var myNodes = new List<Node>
            {
                GetNode(crawlPoint)
            };
            var edges = new List<Edge>();
            while (myNodes.Count != 0)
            {
                foreach (Edge edge in GetMovableEdges(myNodes[0]))
                {
                    edges.Add(edge);
                    if (!edge.Node.Set)
                    {
                        foundPoints[edge.Node.X, edge.Node.Y] = true;
                        edge.Node.Set = true;
                    }
                    edge.Drawn = true;
                    myNodes.Add(edge.Node);
                }
                myNodes.RemoveAt(0);
            }
            var result = 0;
            foreach (bool point in foundPoints)
            {
                if (point)
                    result++;
            }
            foreach (Edge edge in edges)
            {
                edge.Drawn = false;
            }
            return result;
        }

        private MoveEvaluation Crawlv3(List<MoveResult> moves, bool goingBot, int movesLeft)
        {
            var curNode = GetNode(crawlPoint);
            var toReset = false;
            if (!curNode.Set)
            {
                movesLeft--;
                var fast = EvaluateFast(moves);
                if (fast != null)
                {
                    return fast;
                }
                if (movesLeft == 0)
                {
                    return EvaluateBoard(moves);
                }
                goingBot = !goingBot;
                toReset = true;
            }
            var nodes = GetMovableNodes(crawlPoint).ToList();
            if (nodes.Count == 0)
            {
                return new MoveEvaluation(moves)
                {
                    result = goingBot ? GameResult.botPlayerWin : GameResult.topPlayerWin
                };
            }
            MoveEvaluation best = null;
            curNode.Set = true;
            foreach (Node node in nodes)
            {
                moves.Add(MakeMoveResult(GetNode(crawlPoint), node));
                MarkEdge(crawlPoint.X, crawlPoint.Y, moves[moves.Count - 1].addX, moves[moves.Count - 1].addY);
                currentState.MarkOnBoardState(crawlPoint, moves[moves.Count - 1].ConvertToDirection());
                var cand = EvaluatePostition(moves, goingBot, movesLeft);
                best = ChooseBetter(goingBot, best, cand);
                currentState.MarkOnBoardState(crawlPoint, moves[moves.Count - 1].ConvertToDirection(), true);
                MarkEdge(crawlPoint.X, crawlPoint.Y, moves[moves.Count - 1].addX, moves[moves.Count - 1].addY, true);
                moves.RemoveAt(moves.Count - 1);
            }
            if (toReset)
            {
                curNode.Set = false;
            }
            return best;
        }

        private MoveEvaluation EvaluatePostition(List<MoveResult> moves, bool goingBot, int movesLeft)
        {
            if (!myHasher.Contains(currentState, out MoveEvaluation cand))//fix add point mark/unmark
            {
                crawlPoint = MovePoint(crawlPoint, moves[moves.Count - 1]);
                cand = Crawlv3(moves, goingBot, movesLeft);
                currentState.eval = cand ?? throw new System.Exception("returning null");
                myHasher.Add(currentState.Copy());
                crawlPoint = MovePoint(crawlPoint, moves[moves.Count - 1], true);
            }
            return cand;
        }

        /// <summary>
        /// returns how many moves till reaching sth
        /// </summary>
        /// <param name="targetPoint"></param>
        /// <returns></returns>
        private short HowFarIsIt(Point targetPoint)
        {
            short mark = 0;
            if (targetPoint.X == crawlPoint.X && targetPoint.Y == crawlPoint.Y)
                return alreadyThere;

            mark++;//start from 1
            var currentWave = new List<Node>();
            var nextWave = new List<Node>();

            currentWave.Add(GetNode(targetPoint));
            foreach (Node node in GetNodes)
            {
                node.Searched = false;
                node.NodeAdded = false;
            }
            while (currentWave.Any())
            {
                if (SearchForCurrentPoint(currentWave, nextWave))
                {
                    return mark;
                }
                currentWave = nextWave;
                nextWave = new List<Node>();
                mark++;
            }
            return notfound;
        }

        private bool SearchForCurrentPoint(List<Node> currentWave, List<Node> nextWave)
        {
#pragma warning disable CC0006 // Code smell- change to foreach causes collection changed exception
            for (var i = 0; i < currentWave.Count; i++)
#pragma warning restore CC0006 // Use foreach
            {
                var node = currentWave[i];
                if (!node.Searched)
                {
                    foreach (Edge evalutaedEdge in node.Nodes)
                    {
                        if (evalutaedEdge.Drawn)
                            continue;
                        var evalutaedNode = evalutaedEdge.Node;
                        if (evalutaedNode.Searched)
                            continue;
                        if (evalutaedNode.X == crawlPoint.X && evalutaedNode.Y == crawlPoint.Y)
                        {
                            return true;
                        }
                        if (!evalutaedNode.NodeAdded)
                        {
                            evalutaedNode.NodeAdded = true;
                            if (!evalutaedNode.Set)
                                nextWave.Add(evalutaedNode);
                            else
                                currentWave.Add(evalutaedNode);
                        }
                    }
                }
                node.Searched = true;
            }
            return false;
        }
    }
}
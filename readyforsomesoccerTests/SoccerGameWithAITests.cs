﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace readyforsomesoccer.Tests
{
    [TestClass()]
    public class SoccerGameWithAITests
    {
        public static SoccerGameWithAI GetGame(List<Direction> seed)
        {
            return new SoccerGameWithAI(8, 10, seed);
        }

        [TestMethod()]
        public void MakeevaluationTest()
        {
            var ai = GetGame(
                new JavaScriptSerializer().Deserialize<List<Direction>>("[8,3,4,7,6,1,8,9,3,4,8,8,3,4,7,6,1,8,9,9,7]")
                );
            ai.MakeIntelligentMove();

            Assert.AreEqual(ai.GetGameState(), GameResult.botPlayerWin);
        }

        [TestMethod()]
        public void MakeIntelligentMoveTest2()
        {
            var ai = new SoccerGameWithAI(8, 10,
                new JavaScriptSerializer().Deserialize<List<Direction>>("[8,3,4,7,6,1,8,9,3,4,8,8,3,4,7,6,1,8,9,8]")
                );
            ai.botplayersTurn = !ai.botplayersTurn;
            ai.MakeIntelligentMove();

            Assert.AreEqual(ai.GetGameState(), GameResult.noOneWins);
        }

        [TestMethod()]
        [Ignore]
        public void PefrormanceTest()
        {
            var game = GetGame(Seeds.performanceTest);
            game.botplayersTurn = !game.botplayersTurn;
            game.MakeIntelligentMove();
        }

        [TestMethod()]
        public void TestAIMoveAfterWin()
        {
            var game = GetGame(Seeds.straightWinForTop);

            var result = game.MakeIntelligentMove();

            Assert.AreEqual(result.Moves.Count, 0);
        }

        [TestMethod()]
        public void TestNoLooseForBot()
        {
            var game = GetGame(Seeds.oneMoveFromWinForBot);

            game.botplayersTurn = !game.botplayersTurn;
            game.MakeIntelligentMove();

            var state = game.GetGameState();

            Assert.AreEqual(state, GameResult.noOneWins);
        }

        [TestMethod()]
        public void TestNoLooseForTop()
        {
            var game = GetGame(Seeds.oneMoveFromWinForTop);

            game.MakeIntelligentMove();

            var state = game.GetGameState();
            Assert.AreEqual(state, GameResult.noOneWins);
        }

        [TestMethod]
        [Ignore]
        public void TestNull()
        {
            var game = GetGame(Seeds.nullReferenceException);

            game.Move(Direction.upright);

            game.MakeIntelligentMove();
        }

        [TestMethod]
        public void TestSecondNull()
        {
            var game = GetGame(Seeds.secondNullRef);

            game.Move(Direction.upright);
            game.Move(Direction.upleft);

            game.MakeIntelligentMove();
        }

        [TestMethod()]
        public void TestPlayerMoveAfterWin()
        {
            var game = GetGame(Seeds.straightWinForTop);

            var result = game.Move(Direction.upright);

            Assert.AreEqual(result.worked, false);
        }

        [TestMethod()]
        public void TestSafety()
        {
            foreach (var seed in Seeds.TestSafety)
            {
                GetGame(seed);
            }
        }

        [TestMethod()]
        public void TestWinForBot()
        {
            var game = GetGame(Seeds.oneMoveFromWinForBot);

            game.MakeIntelligentMove();

            var state = game.GetGameState();

            Assert.AreEqual(state, GameResult.botPlayerWin);
        }

        [TestMethod()]
        public void TestWinForTop()
        {
            var game = GetGame(Seeds.oneMoveFromWinForTop);
            game.botplayersTurn = !game.botplayersTurn;
            game.MakeIntelligentMove();

            var state = game.GetGameState();

            Assert.AreEqual(state, GameResult.topPlayerWin);
        }

        [TestMethod]
        public void TestArgumentExceptioin()
        {
            var game = GetGame(Seeds.ArgumentException);
            game.Move(Direction.downleft);

            game.MakeIntelligentMove();
        }

        [TestMethod()]
        public void TestWinSeed()
        {
            var game = GetGame(Seeds.straightWinForTop);

            Assert.AreEqual(game.GetGameState(), GameResult.topPlayerWin);
        }

        [TestMethod()]
        public void Testcorner()
        {
            var game = GetGame(Seeds.goToCorner);

            game.MakeIntelligentMove();
        }

        private static class Seeds
        {
            public static readonly List<Direction> goToCorner = new List<Direction> { Direction.up, Direction.upleft, Direction.upleft, Direction.upleft };

            public static readonly List<Direction> bugCornerApproach =
                ParseString("[2, 1, 3, 1, 2, 7, 2, 9, 6, 3, 9, 2, 7, 6, 9, 3, 4, 8, 9, 7, 1, 8, 1, 7, 9, 3, 6, 6, 1, 8, 9, 7, 6, 1, 8, 9, 7, 6, 1]");//todo

            public static readonly List<Direction> nullReferenceException =
                ParseString("[8,1,6,9,3,8,4,4,4,9,3,2,7,8,8,1,6,9,3,8,3,9,7,1,4,4,4,3,6,2,9,4,3,1,4,7,2,3,9,6,9,7,2,9,2,9,4,3,4,4,3,4,1,7,2,6,8,3,4,1,1,4,1,8,4,3,4,9,6,3,2,7,4,2,9,2,9,4,9,2,3,6,9,3,8,1,8,6,8,1,4,9,8]");

            public static readonly List<Direction> secondNullRef =
                ParseString("[8,1,6,9,3,8,4,4,3,8,7,1,6,8,8,1,6,9,3,8,3]");

            public static readonly List<Direction> ArgumentException =
                ParseString("[8,1,6,9,3,8,4,4,3,8,7,1,6,8,8,1]");

            public static readonly List<Direction> oneMoveFromWinForBot =
                ParseString("[8,8,8,8]");

            public static readonly List<Direction> oneMoveFromWinForTop =
                ParseString("[2,2,2,2]");

            public static readonly List<Direction> performanceTest =
                ParseString("[8,1,6,9,3,8,4,4,4,9,3,2,7,8,8,1,6,9,3,8,3,9,7,1,4,4,4,9,7,1,6,7,2,3,2,2,2,4,9,7,9,8,1,6,3,6]");

            public static readonly List<Direction> straightWinForTop =
                ParseString("[2,2,2,2,2,2]");

            public static readonly List<Direction> wrongResult =
                ParseString("[7,4,8,7,9,7,1,3,4,3,6,9,7,4]");

            private static readonly List<string> safetySeeds = new List<string> {
                "[6,6,6,6,6,6]",
            "[4,4,4,4,4,4]",
            "[4,6]",
            "[8,7,3,4,2,9,2,6,7,6,8,1,8,9,3,2,1]"
            };

            public static IEnumerable<List<Direction>> TestSafety
            {
                get
                {
                    foreach (var seed in safetySeeds)
                    {
                        yield return ParseString(seed);
                    }
                }
            }

            public static List<Direction> ParseString(string log)
            {
                return new JavaScriptSerializer().Deserialize<List<Direction>>(log);
            }
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using readyforsomesoccer;
using System.Linq;

namespace readyforsomesoccerTests
{
    [TestClass]
    public class MoveEvaluationTests
    {
        private static MoveEvaluation BotPlayerWin => new MoveEvaluation(Enumerable.Empty<MoveResult>())
        {
            result = GameResult.botPlayerWin
        };

        private static MoveEvaluation TopPlayerWin => new MoveEvaluation(Enumerable.Empty<MoveResult>())
        {
            result = GameResult.topPlayerWin
        };

        [TestMethod]
        public void WinningWinning()
        {
            var botPlayerWin = BotPlayerWin;
            var topPlayerWin = TopPlayerWin;

            Assert.IsTrue(botPlayerWin < topPlayerWin);
            Assert.IsFalse(botPlayerWin > topPlayerWin);

            Assert.IsTrue(topPlayerWin > botPlayerWin);
            Assert.IsFalse(topPlayerWin < botPlayerWin);
        }

        [TestMethod]
        public void WinningRegular()
        {
            var botPlayerWin = BotPlayerWin;
            var topPlayer = new MoveEvaluation(Enumerable.Empty<MoveResult>()) { botDistance = 2, topDistance = 3 };

            Assert.IsTrue(botPlayerWin < topPlayer);
            Assert.IsFalse(botPlayerWin > topPlayer);

            Assert.IsTrue(topPlayer > botPlayerWin);
            Assert.IsFalse(topPlayer < botPlayerWin);
        }

        [TestMethod]
        public void LoosingRegular()
        {
            var botPlayerWin = new MoveEvaluation(Enumerable.Empty<MoveResult>()) {botDistance = 2, topDistance = 3 };
            var topPlayer = TopPlayerWin;

            Assert.IsTrue(botPlayerWin < topPlayer);
            Assert.IsFalse(botPlayerWin > topPlayer);

            Assert.IsTrue(topPlayer > botPlayerWin);
            Assert.IsFalse(topPlayer < botPlayerWin);
        }

        [TestMethod]
        public void CompareResults()
        {
            var botPlayerWin = new MoveEvaluation(Enumerable.Empty<MoveResult>()) { botDistance = 2, topDistance = 3 };
            var topPlayer = new MoveEvaluation(Enumerable.Empty<MoveResult>()) { botDistance = 3, topDistance = 3 };

            Assert.IsTrue(botPlayerWin < topPlayer);
            Assert.IsFalse(botPlayerWin > topPlayer);

            Assert.IsTrue(topPlayer > botPlayerWin);
            Assert.IsFalse(topPlayer < botPlayerWin);
        }

        [TestMethod]
        public void CompareResults2()
        {
            var botPlayerWin = new MoveEvaluation(Enumerable.Empty<MoveResult>()) { botDistance = 2, topDistance = 4 };
            var topPlayer = new MoveEvaluation(Enumerable.Empty<MoveResult>()) { botDistance = 2, topDistance = 3 };

            Assert.IsTrue(botPlayerWin < topPlayer);
            Assert.IsFalse(botPlayerWin > topPlayer);

            Assert.IsTrue(topPlayer > botPlayerWin);
            Assert.IsFalse(topPlayer < botPlayerWin);
        }
    }
}
